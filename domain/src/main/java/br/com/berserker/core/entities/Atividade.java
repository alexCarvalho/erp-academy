package br.com.berserker.core.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class Atividade {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @OneToMany
    private List<Dia> dias;

    public Long getId() {
        return id;
    }

    public List<Dia> getDias() {
        return dias;
    }

    public void setDias(List<Dia> dias) {
        this.dias = dias;
    }
}
