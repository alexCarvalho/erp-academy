package br.com.berserker.core.enums;

public enum TipoPessoa {

    PESSOAFISICA,
    PESSOAJURIDICA;
}
