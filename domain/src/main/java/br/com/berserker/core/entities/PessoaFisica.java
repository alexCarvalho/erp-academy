package br.com.berserker.core.entities;

import br.com.berserker.core.enums.TipoPessoa;

import java.util.Calendar;

public interface PessoaFisica {

    public String getNome();

    public void setNome(String nome);

    public Integer getIdade();

    public void setIdade(Integer idade);

    public Calendar getNascimento();

    public void setNascimento(Calendar nascimento);

    public String getRg();

    public void setRg(String rg);

    public String getCpf();

    public void setCpf(String cpf);

}
