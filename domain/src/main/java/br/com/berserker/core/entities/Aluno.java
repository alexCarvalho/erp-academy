package br.com.berserker.core.entities;

import br.com.berserker.core.enums.TipoPessoa;
import com.sun.istack.internal.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.persistence.Temporal;
import java.util.Calendar;

@Entity
public class Aluno implements PessoaFisica {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull
    private String nome;

    @NotNull
    private Integer idade;

    @NotNull
    @Temporal(TemporalType.DATE)
    private Calendar nascimento;

    @NotNull
    private String rg;

    @NotNull
    private String cpf;

    @Enumerated(EnumType.STRING)
    private TipoPessoa tipoPessoa = TipoPessoa.PESSOAFISICA;

    @OneToOne(fetch = FetchType.LAZY)
    private Endereco endereco;

    public Long getId() {
        return id;
    }

    @Override
    public String getNome() {
        return nome;
    }

    @Override
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public Integer getIdade() {
        return idade;
    }

    @Override
    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    @Override
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    public Calendar getNascimento() {
        return nascimento;
    }

    @Override
    public void setNascimento(Calendar nascimento) {
        this.nascimento = nascimento;
    }

    @Override
    public String getRg() {
        return rg;
    }

    @Override
    public void setRg(String rg) {
        this.rg = rg;
    }

    @Override
    public String getCpf() {
        return cpf;
    }

    @Override
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
