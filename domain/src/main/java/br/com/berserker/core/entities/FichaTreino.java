package br.com.berserker.core.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class FichaTreino {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String descricao;

    @OneToOne
    private Aluno aluno;

    @OneToOne
    private Funcionario funcionario;

    @OneToMany
    private List<Atividade> atividades;

    public Long getId() {
        return id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public List<Atividade> getAtividades() {
        return atividades;
    }

    public void setAtividades(List<Atividade> atividades) {
        this.atividades = atividades;
    }
}
