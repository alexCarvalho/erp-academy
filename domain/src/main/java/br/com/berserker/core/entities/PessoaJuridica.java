package br.com.berserker.core.entities;

import br.com.berserker.core.enums.TipoPessoa;

/**
 * Created by renato on 19/06/17.
 */
public interface PessoaJuridica {

    public String getRazaoSocial();

    public void setRazaoSocial(String razaoSocial);

    public String getNomeFantasia();

    public void setNomeFantasia(String nomeFantasia);

    public String getInscricaoMunicipal();

    public void setInscricaoMunicipal(String inscricaoMunicipal);

    public String getInscricaoEstadual();

    public void setInscricaoEstadual(String inscricaoEstadual);

    public String getCnpj();

    public void setCnpj(String cnpj);

    public TipoPessoa getTipoPessoa ( );


}
