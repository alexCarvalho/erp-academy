package br.com.berserker.web.controller;

import br.com.berserker.core.entities.Aluno;
import br.com.berserker.core.enums.TipoPessoa;
import br.com.berserker.web.service.AlunoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Calendar;

@RestController
public class AlunoController {

    @Autowired
    private AlunoService alunoService;

    @GetMapping("/aluno")
    public void saveAluno(){
        System.out.println("Aluno chamado !!!");

        Aluno aluno = new Aluno();
        aluno.setNome("Renato");
        aluno.setCpf("22703395841");
        aluno.setIdade(33);
        aluno.setNascimento(Calendar.getInstance());
        aluno.setRg("321507903");

        alunoService.saveAluno(aluno);
    }
}
