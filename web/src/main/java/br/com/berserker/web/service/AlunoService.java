package br.com.berserker.web.service;

import br.com.berserker.core.entities.Aluno;
import br.com.berserker.core.repository.AlunoRepository;
import org.springframework.stereotype.Service;

@Service
public class AlunoService {

    private AlunoRepository alunoRepository;

    public AlunoService(AlunoRepository alunoRepository) {
        this.alunoRepository = alunoRepository;
    }

    public void saveAluno(Aluno aluno) {
        alunoRepository.save(aluno);
    }
}
